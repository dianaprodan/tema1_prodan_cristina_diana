function Exception (str){
this.message=str;
this.name="Exception"
}

function distance (first,second){
        if(Array.isArray(first)&&Array.isArray(second))
        {

            if(first.length==0 && second.length==0)
             return 0;  // daca sunt vide returneaza 0
                       else{

            first=Array.from(new Set(first)) //creez un array cu set, fara duplicate
            second =Array.from(new Set(second))

              // daca nu sunt vide, distanta de la first la second si invers
            let dist1=first.filter(value=> second.indexOf(value)==-1).length 
           // console.log(first + "  " + dist1)


            let dist2 = second.filter(value1=> first.indexOf(value1)==-1).length
           //console.log(second + "  " + dist2)

            return dist1+dist2
                            }
        }
        else
                throw new Exception("InvalidType")  //daca nu e array 

}

module.exports.distance = distance